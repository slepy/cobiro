<?php

namespace App\Services;

use App\Exceptions\MenuFieldExistException;
use App\Exceptions\MenuNotFoundException;
use App\Menu as MenuModel;
use App\Repositories\Menu as MenuRepository;
use Illuminate\Support\Facades\Cache;

class Menu
{
    /**
     * @var App\Repositories\Menu
     */
    private $menuRepository;

    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    /**
     * Add new menu if not exist and add to cache
     *
     * @param array $data
     * @return MenuModel
     * @throws MenuFieldExistException
     */
    public function add(array $data): MenuModel
    {
        $field = $data['field'];

        if (!empty($this->get($field))) {
            throw new MenuFieldExistException('Menu ' . $field . ' already exists');
        }
        $menu = $this->menuRepository->add($data);
        Cache::put('menu-' . $field, $menu);

        return $menu;
    }

    /**
     * Get menu form cache or repository if exist
     *
     * @param string $field
     * @return MenuModel|null
     */
    public function get(string $field): ?MenuModel
    {
        $menu = null;

        if (Cache::has('menu-' . $field)) {
            $menu = Cache::get('menu-' . $field);
        } else {
            $menu = $this->menuRepository->get($field);
            Cache::put('menu-' . $field, $menu);
        }

        return $menu;
    }

    /**
     * Update menu if exist and update cache
     *
     * @param array $data
     * @param string $field
     * @return MenuModel
     */
    public function update(array $data, string $field): MenuModel
    {
        $this->menuExist($field);

        $menu = $this->menuRepository->update($data, $field);
        Cache::forget('menu-' . $field);
        Cache::put('menu-' . $data['field'], $menu);

        return $menu;
    }

    /**
     * Remove menu and remove from cache
     *
     * @param string $field
     * @return void
     */
    public function delete(string $field): void
    {
        $this->menuExist($field);
        $this->menuRepository->delete($field);
        Cache::forget('menu-' . $field);
    }

    /**
     * Check if menu exist, if not throw exception
     *
     * @param string $field
     * @return void
     * @throws MenuNotFoundException
     */
    private function menuExist(string $field): void
    {
        if (empty($this->get($field))) {
            throw new MenuNotFoundException('Menu ' . $field . ' Not found');
        }
    }
}
