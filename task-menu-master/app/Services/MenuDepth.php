<?php

namespace App\Services;

use App\Repositories\Menu as MenuRepository;
use App\Repositories\Item as ItemRepository;

class MenuDepth
{
    /**
     * @var App\Repositories\Menu
     */
    private $menuRepository;

    /**
     * @var App\Repositories\Item
     */
    private $itemRepository;

    public function __construct(MenuRepository $menuRepository, ItemRepository $itemRepository)
    {
        $this->menuRepository = $menuRepository;
        $this->itemRepository = $itemRepository;
    }
    /**
     * Get menu depth
     *
     * @param string $field
     * @return int|null
     */
    public function get(string $menu): ?int
    {
        $menuModel = $this->menuRepository->get($menu);
        
        return $this->itemRepository->getDepth($menuModel->id);
    }

}
