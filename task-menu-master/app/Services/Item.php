<?php

namespace App\Services;

use App\Exceptions\ItemFieldExistException;
use App\Exceptions\ItemNotFoundException;
use App\Item as ItemModel;
use App\Repositories\Item as ItemRepository;
use Illuminate\Support\Facades\Cache;

class Item
{
    /**
     * @var App\Repositories\Item
     */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * Add new item
     *
     * @param array $data
     * @return ItemModel
     * @throws ItemFieldExistException
     */
    public function add(array $data): ItemModel
    {
        $field = $data['field'];

        if (!empty($this->get($field))) {
            throw new ItemFieldExistException('Item ' . $field . ' already exists');
        }
        
        $item = $this->itemRepository->add($data);
        Cache::put('item-' . $field, $item);

        return $item;
    }

    /**
     * Get item form cache or repository if exist
     *
     * @param string $field
     * @return ItemModel|null
     */
    public function get(string $field): ?ItemModel
    {
        $item = null;

        if (Cache::has('item-' . $field)) {
            $item = Cache::get('item-' . $field);
        } else {
            $item = $this->itemRepository->get($field);
            Cache::put('item-' . $field, $item);
        }

        return $item;
    }

    /**
     * Update item if exist and update cache
     *
     * @param array $data
     * @param string $field
     * @return ItemModel
     */
    public function update(array $data, string $field): ItemModel
    {
        $this->itemExist($field);
        //We should also modify menuitem
        $item = $this->itemRepository->update($data, $field);
        Cache::forget('item-'. $field);
        Cache::put('item-' . $data['field'], $item);

        return $item;
    }

    /**
     * Remove item and remove from cache
     *
     * @param string $field
     * @return void
     */
    public function delete(string $field): void
    {
        $this->itemExist($field);
        $this->itemRepository->delete($field);
        Cache::forget('item-' . $field);
    }

    /**
     * Check if item exist, if not throw exception
     *
     * @param string $field
     * @return void
     * @throws ItemNotFoundException
     */
    private function itemExist(string $field): void
    {
        //maybe name of the function should be 'checkIfItemExist'
        if (empty($this->get($field))) {
            throw new MenuNotFoundException('Item ' . $field . ' Not found');
        }
    }
}
