<?php

namespace App\Services;

use App\Exceptions\MenuFieldExistException;
use App\Exceptions\MenuNotFoundException;
use App\Repositories\Menu as MenuRepository;
use App\Repositories\Item as ItemRepository;
use Illuminate\Support\Facades\Cache;

class MenuItem
{
    /**
     * @var App\Repositories\Menu
     */
    private $menuRepository;

    /**
     * @var App\Repositories\Item
     */
    private $itemRepository;

    public function __construct(MenuRepository $menuRepository, ItemRepository $itemRepository)
    {
        $this->menuRepository = $menuRepository;
        $this->itemRepository = $itemRepository;
    }

    /**
     * Add new Items in menu
     *
     * @param array $data
     * @param string $menu
     * @return void
     */
    public function add(array $data, string $menu): void
    {
        $this->menuExist($menu);

        $menu = $this->menuRepository->get($menu);
        $preparedData = $this->prepareData($data, $menu->id, $menu->max_depth, $menu->max_children);

        foreach ($preparedData as &$value) {
            if ($value['parent']) {
                $value['parent'] = $this->itemRepository->get($value['parent'])['id'];
            }
            (new ItemRepository(new \App\Item))->add($value);
        }
        Cache::forget('menuItems-' . $menu);
    }

    /**
     * Get items of menu form cache or repository if exist and build tree
     *
     * @param string $field
     * @return array|null
     */
    public function get(string $menu): ?array
    {
        $items = null;

        if (Cache::has('menuItems-' . $menu)) {
            $items = Cache::get('menuItems-' . $menu);
        } else {
            $menuModel = $this->menuRepository->get($menu);
            $items = $this->itemRepository->getAll($menuModel->id);
            Cache::put('menuItems-' . $menu, $items);
        }

        $tree = $this->buildTree($items);

        return $tree;
    }

    /**
     * Remove menu items and remove from cache
     *
     * @param string $menu
     * @return void
     */
    public function delete(string $menu): void
    {
        $this->menuExist($menu);
        $menuModel = $this->menuRepository->get($menu);
        $this->itemRepository->deleteByMenuId($menuModel->id);
        
        Cache::forget('menuItems-' . $menu);
    }

    /**
     * Check if menu exist, if not throw exception
     *
     * @param string $field
     * @return void
     * @throws MenuNotFoundException
     */
    private function menuExist(string $field): void
    {
        if (!($this->menuRepository->get($field))) {
            throw new MenuNotFoundException('Field ' . $field . ' Not found');
        }
    }

    /**
     * Preparing data do add to Database/// sorry for so many params
     * Making tree flat
     *
     * @param array $data
     * @param integer $menu
     * @param integer $maxDepth
     * @param integer $maxChildren
     * @param integer $depth
     * @param string $parent
     * @return array
     * @throws MaxChildrenExceedException
     * @throws MaxDepthExceedException
     */
    private function prepareData(
        array $data,
        int $menu,
        int $maxDepth,
        int $maxChildren,
        int $depth = 1,
        string $parent = null
    ): array
    {
        $preparedData = [];
        $childrenCount = 1;
        foreach ($data as $value) {
            /**check if maxChildren is exceeded */
            if ($maxChildren < $childrenCount) {
                throw new MaxChildrenExceedException('Max children count is ' . $maxChildren);
            }
            $childrenCount++;
            $preparedData[] = [
                'field' => $value['field'],
                'menu' => $menu,
                'depth' => $depth,
                'parent' => $parent
            ];

            if (!empty($value['children']) && is_array($value['children'])) {
                $newDepth = $depth + 1;

                /**check if maxdepth is exceeded */
                if ($maxDepth < $newDepth) {
                    throw new MaxDepthExceedException('Max depth is ' . $maxDepth);
                }

                $preparedData = array_merge(
                    $preparedData,
                    $this->prepareData($value['children'], $menu, $maxDepth, $maxChildren, $newDepth, $value['field'])
                );
            }
        }

        return $preparedData;
    }

    /**
     * Build tree of items
     *
     * @param array $elements
     * @param integer $parentId
     * @return array
     */
    private function buildTree(array &$elements, int $parentId = null): array
    {
        $branch = [];

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                $newElement['field'] = $element['field'];

                if ($children) {
                    $newElement['children'] = $children;
                }
                $branch[] = $newElement;
                unset($element);
                unset($newElement);
            }
        }

        return $branch;
    }
}
