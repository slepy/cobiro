<?php

namespace App\Repositories;

use \App\Item as ItemModel;
use Illuminate\Support\Facades\DB;

class Item
{
    /**
     * @var \App\Item
     */
    private $model;

    public function __construct(ItemModel $model)
    {
        $this->model = $model;
    }

    /**
     * Add new Items
     *
     * @param array $data
     * @return ItemModel
     */
    public function add(array $data): ItemModel
    {
        $this->model->field = $data['field'];
        $this->model->menu = $data['menu'] ?? null;
        $this->model->parent = $data['parent'] ?? null;
        $this->model->depth = $data['depth'] ?? null;
        $this->model->save();
        
        return $this->model;
    }

    /**
     * Get Items by Menu Id
     *
     * @param integer $menuId
     * @return array
     */
    public function getAll(int $menuId): array
    {
        return $this->model->where('menu', $menuId)->get()->toArray();
    }

    /**
     * Get Item
     *
     * @param string $field
     * @return ItemModel|null
     */
    public function get(string $field): ?ItemModel
    {
        return $this->model->where('field', '=', $field)->first();
    }

    /**
     * Update menu
     *
     * @param array $data
     * @param string $field
     * @return MenuModel
     */
    public function update(array $data, string $field): ItemModel
    {
        $menu = $this->get($field);
        $menu->field = $data['field'] ?? $menu->field;
        $menu->save();

        return $menu;
    }

    /**
     * Remove Item
     *
     * @param string $field
     * @return void
     */
    public function delete(string $field): void
    {
        $this->get($field)->delete();
    }

    /**
     * Remove Item By Menu Id
     *
     * @param string $field
     * @return void
     */
    public function deleteByMenuId(int $menuId): void
    {
        DB::table('items')->where('menu', '=', $menuId)->delete();
    }

    
    /**
     * Remove Item By Menu Id
     *
     * @param string $field
     * @return int
     */
    public function getDepth(int $menuId): ?int
    {
        return DB::table('items')->where('menu', '=', $menuId)->max('depth');
    }
}
