<?php

namespace App\Repositories;

use \App\Menu as MenuModel;

class Menu
{
    /**
     * @var \App\Menu
     */
    private $model;

    public function __construct(MenuModel $model)
    {
        $this->model = $model;
    }

    /**
     * Add new menu
     *
     * @param array $data
     * @return MenuModel
     */
    public function add(array $data): MenuModel
    {
        $this->model->field = $data['field'];
        $this->model->max_depth = $data['max_depth'];
        $this->model->max_children = $data['max_children'];
        $this->model->save();
        
        return $this->model;
    }

    /**
     * Get Menu
     *
     * @param string $field
     * @return MenuModel|null
     */
    public function get(string $field): ?MenuModel
    {
        return $this->model->where('field', '=', $field)->first();
    }

    /**
     * Update menu
     *
     * @param array $data
     * @param string $field
     * @return MenuModel
     */
    public function update(array $data, string $field): MenuModel
    {
        $menu = $this->get($field);
        $menu->field = $data['field'] ?? $menu->field;
        $menu->max_depth = $data['max_depth'] ?? $menu->max_depth;
        $menu->max_children = $data['max_children'] ?? $menu->max_children;
        $menu->save();

        return $menu;
    }

    /**
     * Remove Menu
     *
     * @param string $field
     * @return void
     */
    public function delete(string $field): void
    {
        $this->get($field)->delete();
    }
}
