<?php

namespace App\Providers;

use App\Services\Menu as MenuService;
use App\Services\MenuItem as MenuItemService;
use App\Services\ItemItem as ItemService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MenuService::class, function ($app) {
            return new MenuService($app->make('App\Repositories\Menu'));
        });
        
        $this->app->bind(MenuItemService::class, function ($app) {
            return new MenuItemService(
                $app->make('App\Repositories\Menu'),
                $app->make('App\Repositories\Item')
            );
        });
        
        $this->app->bind(MenuDepth::class, function ($app) {
            return new MenuDepth(
                $app->make('App\Repositories\Menu'),
                $app->make('App\Repositories\Item')
            );
        });
        
        $this->app->bind(ItemService::class, function ($app) {
            return new ItemService($app->make('App\Repositories\Item'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
