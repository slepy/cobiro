<?php

namespace App\Http\Controllers;

use App\Exceptions\BadRequestException;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->json();
        
        if (!$data->has('field')) {
            throw new BadRequestException('Invalid argument');
        }
        $menuService = resolve('App\Services\Menu');

        /* $data->all() convert to array, We can use (object) to work with object */
        $response = $menuService->add($data->all());

        return response()->json([
            'field' => $response->field,
            'max_depth' =>  $response->max_depth,
            'max_children' => $response->max_children
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function show($menu)
    {
        if (!$menu) {
            throw new BadRequestException('Invalid argument');
        }
        $response = resolve('App\Services\Menu')->get($menu);

        return response()->json([
            'field' => $response->field,
            'max_depth' =>  $response->max_depth,
            'max_children' => $response->max_children
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $menu)
    {
        $data = $request->json()->all();
        if (empty($data) || !$menu) {
            throw new BadRequestException('Invalid argument');
        }
        $response = resolve('App\Services\Menu')->update($data, $menu);

        return response()->json([
            'field' => $response->field,
            'max_depth' =>  $response->max_depth,
            'max_children' => $response->max_children
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($menu)
    {
        if (!$menu) {
            throw new BadRequestException('Invalid argument');
        }
        
        resolve('App\Services\Menu')->delete($menu);

        return response()->json([true], 200);
    }
}
