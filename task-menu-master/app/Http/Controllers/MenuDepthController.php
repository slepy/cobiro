<?php

namespace App\Http\Controllers;

class MenuDepthController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function show($menu)
    {
        if (!$menu) {
            throw new BadRequestException('Invalid argument');
        }

        $menuDepthService = resolve('App\Services\MenuDepth');
        $response = $menuDepthService->get($menu);

        return response()->json(['depth' => $response]);
    }
}
