<?php

namespace App\Http\Controllers;

use App\Exceptions\BadRequestException;
use Illuminate\Http\Request;

class MenuItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $menu)
    {
        $data = $request->json()->all();
        if (empty($data) || !$menu) {
            throw new BadRequestException('Invalid argument');
        }

        $menuItemService = resolve('App\Services\MenuItem');
        $menuItemService->add($data, $menu);
        $response = $menuItemService->get($menu);

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function show($menu)
    {
        if (!$menu) {
            throw new BadRequestException('Invalid argument');
        }

        $menuItemService = resolve('App\Services\MenuItem');
        $response = $menuItemService->get($menu);

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($menu)
    {
        if (!$menu) {
            throw new BadRequestException('Invalid argument');
        }

        $menuItemService = resolve('App\Services\MenuItem');
        $menuItemService->delete($menu);

        return response()->json([true], 200);
    }
}
