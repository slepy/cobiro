<?php

namespace App\Http\Controllers;

use App\Exceptions\BadRequestException;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->json();
        
        if (!$data->has('field')) {
            throw new BadRequestException('Invalid argument');
        }
        $itemService = resolve('App\Services\Item');
        $response = $itemService->add($data->all());

        return response()->json([
            'field' => $response->field
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {
        if (!$item) {
            throw new BadRequestException('Invalid argument');
        }
        $response = resolve('App\Services\Item')->get($item);

        return response()->json([
            'field' => $response['field']
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $item)
    {
        $data = $request->json()->all();
        if (empty($data) || !$item) {
            throw new BadRequestException('Invalid argument');
        }
        $response = resolve('App\Services\Item')->update($data, $item);

        return response()->json([
            'field' => $response->field
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($item)
    {
        if (!$item) {
            throw new BadRequestException('Invalid argument');
        }
        
        resolve('App\Services\Item')->delete($item);

        return response()->json([true], 200);
    }
}
