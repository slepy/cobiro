<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    
    public function render($request, \Exception $exception)
    {
        if ($exception instanceof BadRequestException
        || $exception instanceof MenuFieldExistException
        || $exception instanceof ItemFieldExistException) {
            return response()->json(['error' => $exception->getMessage()], 400);
        }

        if ($exception instanceof MenuNotFoundException
        || $exception instanceof ItemNotFoundException) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }

        dd($exception);
        return parent::render($request, $exception);
    }
}
